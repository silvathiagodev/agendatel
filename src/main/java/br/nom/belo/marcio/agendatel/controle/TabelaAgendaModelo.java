package br.nom.belo.marcio.agendatel.controle;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.nom.belo.marcio.agendatel.db.MysqlCon;
import br.nom.belo.marcio.agendatel.modelo.Contato;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public class TabelaAgendaModelo extends AbstractTableModel {

    private List<Contato> lista=new ArrayList<>();

    public TabelaAgendaModelo() {
    }

    public Contato getContato(int linha) {
        return lista.get(linha);
    }
    
    @Override
    public int getRowCount() {
        if(lista!=null) return lista.size();
        else return 0;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: 
                return "Nome";
            case 1:
                return "Telefone";
            default:
                return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Contato contato=lista.get(rowIndex);
        switch(columnIndex) {
            case 0:
                return contato.getNome();
            case 1:
                return contato.getTelefone();
            default:
                return null;
        }
    }

    public void remove(int linha) {
        lista.remove(linha);
        fireTableDataChanged();
    }

    public void setContato(Contato contatoEditado, int linha) {
        lista.set(linha, contatoEditado);
        fireTableDataChanged();
    }
    
    public void adicionarContato(Contato contato) {
        lista.add(contato);
        fireTableDataChanged();
    }

    void carregarContatos() {
        Connection db = null;
        try {
            db = MysqlCon.getConnection("agendatel");
            Statement statement = db.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM contatos");
            while (result.next()) {
                lista.add(new Contato(result.getInt("id"), result.getString("nome"), result.getString("telefone")));
            }
            fireTableDataChanged();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro de persistência.");
        } finally {
            if(db != null) {
                try {
                    db.close();
                } catch (SQLException ex) {
                    Logger.getLogger(TabelaAgendaModelo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

}
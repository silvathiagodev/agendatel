package br.nom.belo.marcio.agendatel.db;

import java.sql.*;

public class MysqlCon {

    public static Connection getConnection(String databaseName) throws SQLException {
        final String JDBC_URL = "jdbc:mysql://localhost:3306/" + databaseName;
        return DriverManager.getConnection(JDBC_URL, "root", "1234");
    }


}

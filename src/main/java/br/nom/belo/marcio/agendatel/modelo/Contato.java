package br.nom.belo.marcio.agendatel.modelo;

import br.nom.belo.marcio.agendatel.db.MysqlCon;

import java.io.Serializable;
import java.sql.*;

public class Contato implements Serializable {
    private final int id;
    private String nome;
    private String telefone;

    public Contato(int id, String nome, String telefone) {
        this.id = id;
        setNome(nome);
        setTelefone(telefone);
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public static Contato save(String nome, String telefone) {
        try (Connection db = MysqlCon.getConnection("agendatel")) {


            //insere no banco
            PreparedStatement insert = db.prepareStatement("INSERT INTO contatos (`nome`, `telefone`) VALUES (?, ?)");
            insert.setString(1, nome);
            insert.setString(2,telefone);
            insert.execute();

            // pega o id do novo registro
            Statement statement = db.createStatement();
            ResultSet select = statement.executeQuery("SELECT LAST_INSERT_ID() as id");
            select.next();
            int id = select.getInt("id");
            //cria novo objeto;
            return new Contato(id, nome, telefone);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public boolean edit(String nome, String telefone) {
        try (Connection db = MysqlCon.getConnection("agendatel")) {
            PreparedStatement update = db.prepareStatement("UPDATE contatos SET nome=?, telefone=? WHERE id=?");
            update.setString(1,nome);
            update.setString(2,telefone);
            update.setInt(3,this.getId());
            update.executeUpdate();
            setNome(nome);
            setTelefone(telefone);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean remove() {
        try (Connection db = MysqlCon.getConnection("agendatel")) {
            PreparedStatement statement = db.prepareStatement("DELETE FROM contatos where id = ?");
            statement.setInt(1,getId());
            statement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
